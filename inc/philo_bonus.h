#ifndef PHILO_BONUS_H
# define PHILO_BONUS_H

# include <pthread.h>
# include <sys/time.h>
# include <semaphore.h>

#define PHILO_OUT "/philout"
#define PHILO_PRINT "/philoprint"
#define PHILO_FORK "/philofork"
#define PHILO_MEALS "/philomeals"


typedef struct s_data
{
	int				philo_num;
	int				num;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				meals_num;
	int				should_die;
	long long		last_meal;
	sem_t			*sem_meals;
	sem_t			*sem_print;
	sem_t			*sem_out;
}	t_data;


int		run(t_data *data);
void	logger(char *format, int nbr, sem_t *sem);
void	unlink_all(int close, t_data *data, sem_t *sem);
void	philo_cycle(t_data *data, sem_t *sem);

#endif
