/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/04 03:24:05 by iait-bel          #+#    #+#             */
/*   Updated: 2022/06/05 14:00:41 by bella            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include <sys/time.h>
int			ft_strlen(char *s);
void		ft_putstr(int fd, char *s);
int			ft_atoi(char *str, int *res);
long		time_diff(struct timeval t1, struct timeval t2);
void		ft_usleep(int n);
long		get_time(struct timeval tp);
long long	get_rel_time(void);

#endif
