/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/04 03:25:36 by iait-bel          #+#    #+#             */
/*   Updated: 2022/06/05 14:02:49 by bella            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <pthread.h>
# include <sys/time.h>

typedef struct s_data
{
	int				philo_num;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				meals_num;
	pthread_mutex_t	print_mutex;
	pthread_mutex_t	*mutex;
	_Atomic int		out;
}	t_data;

typedef struct s_philo {
	int				nbr;
	int				meals_eaten;
	pthread_t		thread;
	pthread_mutex_t	*right;
	pthread_mutex_t	*left;
	long long		last_meal;
	t_data			*data;
}	t_philo;

t_philo	*run(t_data *data);
void		logging(char *format, t_philo *philo, int is_eat);

#endif
