/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gate.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/04 03:16:42 by iait-bel          #+#    #+#             */
/*   Updated: 2022/06/05 03:25:10 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include "utils.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>

void	*routine(void *ptr)
{
	t_philo	*philo;

	philo = ptr;
	while (!philo->data->out)
	{
		logging("%.11ld %d is thinking\n", philo, 0);
		if (philo->nbr % 2 == 0)
			pthread_mutex_lock(philo->left);
		else
			pthread_mutex_lock(philo->right);
		logging("%.11ld %d has taken a fork\n", philo, 0);
		if (philo->nbr % 2 == 0)
			pthread_mutex_lock(philo->right);
		else
			pthread_mutex_lock(philo->left);
		logging("%.11ld %d has taken a fork\n", philo, 0);
		(philo->meals_eaten)++;
		logging("%.11ld %d is eating\n", philo, 1);
		ft_usleep(philo->data->time_to_eat * 1000);
		pthread_mutex_unlock(philo->left);
		pthread_mutex_unlock(philo->right);
		logging("%.11ld %d is sleeping\n", philo, 0);
		ft_usleep(philo->data->time_to_sleep * 1000);
	}
	return (NULL);
}

void	destroy(t_data *data, t_philo *philos, int n)
{
	int	i;

	i = 0;
	while (i < n)
		pthread_mutex_destroy(data->mutex + i++);
	free(philos);
	free(data->mutex);
}

t_philo	*setup_philos(t_data *data)
{
	int		i;
	t_philo	*philos;

	philos = malloc(sizeof(t_philo) * (data->philo_num));
	data->mutex = malloc(sizeof(pthread_mutex_t) * (data->philo_num));
	if (philos == NULL || data->mutex == NULL)
		return (destroy(data, philos, 0), NULL);
	if (pthread_mutex_init(&data->print_mutex, NULL) != 0)
		return (destroy(data, philos, 0), NULL);
	i = 0;
	philos[0].right = data->mutex + data->philo_num - 1;
	while (i < data->philo_num)
	{
		philos[i].data = data;
		philos[i].nbr = i;
		philos[i].left = data->mutex + i;
		if (i != 0)
			philos[i].right = data->mutex + i - 1;
		if (pthread_mutex_init(philos[i++].left, NULL) != 0)
			return (destroy(data, philos, i), NULL);
	}
	return (philos);
}

t_philo	*run(t_data *data)
{
	int		i;
	t_philo	*philos;

	philos = setup_philos(data);
	if (philos == NULL)
		return (ft_putstr(2, "error\n"), NULL);
	i = 0;
	while (i < data->philo_num)
	{
		philos[i].last_meal = 0;
		if (pthread_create(&philos[i].thread, NULL, &routine, philos + i) != 0)
		{
			ft_putstr(2, "cannot create a thread\n");
			return (free(philos), NULL);
		}
		pthread_detach(philos[i++].thread);
	}
	return (philos);
}
