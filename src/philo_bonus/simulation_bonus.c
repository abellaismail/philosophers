/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   simulation_bonus.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bella </var/spool/mail/bella>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/05 15:39:12 by bella             #+#    #+#             */
/*   Updated: 2022/06/06 09:08:26 by bella            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_bonus.h"
#include "utils.h"
#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <semaphore.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

void	*exit_proccess(void *ptr)
{
	t_data	*data;

	data = ptr;
	sem_wait(data->sem_out);
	if (data->should_die)
		exit(0);
	return (NULL);
}

void	*routine(void *ptr)
{
	t_data	*data;
	int		i;

	data = ptr;
	while (1)
	{
		if (get_rel_time() - data->last_meal > data->time_to_die)
		{
			sem_wait(data->sem_print);
			data->should_die = 0;
			i = 0;
			while (i < data->philo_num)
			{
				sem_post(data->sem_out);
				i++;
			}
			logger("%.11d %d died\n", data->num, NULL);
			exit(0);
		}
		usleep(100);
	}
	return (NULL);
}

void	run_proccess(sem_t *sem, t_data *data, int nbr)
{
	pthread_t	th_exit;
	pthread_t	th_routine;

	data->num = nbr;
	if (pthread_create(&th_routine, NULL, routine, data) != 0)
		return ;
	if (pthread_create(&th_exit, NULL, exit_proccess, data) != 0)
		return ;
	pthread_detach(th_routine);
	pthread_detach(th_exit);
	while (1)
		philo_cycle(data, sem);
}

void	check_meals(t_data *data)
{
	int	i;

	i = 0;
	while (i < data->philo_num)
	{
		sem_wait(data->sem_meals);
		i++;
	}
	i = 0;
	while (i < data->philo_num)
	{
		sem_post(data->sem_out);
		i++;
	}
}

int	run(t_data *data)
{
	int		i;
	pid_t	pid;
	sem_t	*sem;
	int		status;

	sem = NULL;
	unlink_all(0, data, sem);
	sem = sem_open(PHILO_FORK, O_CREAT, S_IRUSR | S_IWUSR, data->philo_num);
	data->sem_print = sem_open(PHILO_PRINT, O_CREAT, S_IRUSR | S_IWUSR, 1);
	data->sem_out = sem_open(PHILO_OUT, O_CREAT, S_IRUSR | S_IWUSR, 0);
	data->sem_meals = sem_open(PHILO_MEALS, O_CREAT, S_IRUSR | S_IWUSR, 0);
	if (sem == NULL || data->sem_out == NULL || data->sem_print == NULL)
		return (unlink_all(1, data, sem),
			ft_putstr(2, "can't open semaphore"), 0);
	i = 0;
	while (i < data->philo_num)
	{
		pid = fork();
		if (pid == -1)
			return (ft_putstr(2, "can't fork"), 0);
		if (pid == 0)
			run_proccess(sem, data, i);
		i++;
	}
	if (data->meals_num != -1)
		check_meals(data);
	while (1)
	{
		pid = waitpid(-1, &status, WNOHANG);
		if (status && WIFEXITED(status))
		{
			unlink_all(1, data, sem);
			kill(0, SIGINT);
		}
		if (pid == -1)
			break ;
	}
	unlink_all(1, data, sem);
	exit(0);
}
