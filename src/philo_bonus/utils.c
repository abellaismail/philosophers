#include <semaphore.h>
#include "philo_bonus.h"
#include <utils.h>
#include <stdio.h>
#include <pthread.h>

void	logger(char *format, int nbr, sem_t *sem)
{
	if (sem)
		sem_wait(sem);
	printf(format, get_rel_time(), nbr);
	if (sem)
		sem_post(sem);
}

void	unlink_all(int close, t_data *data, sem_t *sem)
{
	sem_unlink(PHILO_FORK);
	sem_unlink(PHILO_PRINT);
	sem_unlink(PHILO_OUT);
	sem_unlink(PHILO_MEALS);
	if (!close)
		return ;
	sem_close(sem);
	sem_close(data->sem_print);
	sem_close(data->sem_out);
	sem_close(data->sem_meals);
}

void philo_cycle(t_data *data, sem_t *sem)
{
	logger("%.11d %d is thinking\n", data->num, data->sem_print);
	if (data->num % 2 == 0)
		ft_usleep(100);
	sem_wait(sem);
	logger("%.11d %d has taken a fork\n", data->num, data->sem_print);
	sem_wait(sem);
	logger("%.11d %d has taken a fork\n", data->num, data->sem_print);
	data->meals_num--;
	logger("%.11d %d is eating\n", data->num, data->sem_print);
	if (data->meals_num == 0)
		sem_post(data->sem_meals);
	data->last_meal = get_rel_time();
	ft_usleep(data->time_to_eat * 1000);
	sem_post(sem);
	sem_post(sem);
	logger("%.11d %d is sleeping\n", data->num, data->sem_print);
	ft_usleep(data->time_to_sleep * 1000);
}
