/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bella </var/spool/mail/bella>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/05 15:43:46 by bella             #+#    #+#             */
/*   Updated: 2022/06/06 08:49:43 by bella            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "philo_bonus.h"
#include "utils.h"

int	data_setup(t_data *data, int ac, char *av[])
{
	data->should_die = 0;
	data->meals_num = -1;
	data->last_meal = 0;
	if (!ft_atoi(av[1], &data->philo_num) || data->philo_num == 0)
		return (ft_putstr(2, "invalid argument1\n"), 0);
	if (!ft_atoi(av[2], &data->time_to_die))
		return (ft_putstr(2, "invalid argument2\n"), 0);
	if (!ft_atoi(av[3], &data->time_to_eat))
		return (ft_putstr(2, "invalid argument3\n"), 0);
	if (!ft_atoi(av[4], &data->time_to_sleep))
		return (ft_putstr(2, "invalid argument4\n"), 0);
	if (ac == 6)
		if (!ft_atoi(av[5], &data->meals_num) || data->meals_num == 0)
			return (ft_putstr(2, "invalid argument5\n"), 0);
	return (1);
}

int	main(int ac, char *av[])
{
	t_data		data;

	if (ac < 5 || ac > 6)
	{
		if (ac > 0)
			ft_putstr(2, av[0]);
		else
			ft_putstr(2,
				"philo philo_nbr time2die time2eat time2sleep [meals]");
		ft_putstr(2, ": Not enough arguments\n");
		return (1);
	}
	if (data_setup(&data, ac, av) == 0)
		return (1);
	run(&data);
}
