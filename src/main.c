/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/04 00:56:35 by iait-bel          #+#    #+#             */
/*   Updated: 2022/06/05 15:15:03 by bella            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "philo.h"
#include "utils.h"

int	data_setup(t_data *data, int ac, char *av[])
{
	data->meals_num = -1;
	data->out = 0;
	if (!ft_atoi(av[1], &data->philo_num) || data->philo_num == 0)
		return (ft_putstr(2, "invalid argument1\n"), 0);
	if (!ft_atoi(av[2], &data->time_to_die))
		return (ft_putstr(2, "invalid argument2\n"), 0);
	if (!ft_atoi(av[3], &data->time_to_eat))
		return (ft_putstr(2, "invalid argument3\n"), 0);
	if (!ft_atoi(av[4], &data->time_to_sleep))
		return (ft_putstr(2, "invalid argument4\n"), 0);
	if (ac == 6)
		if (!ft_atoi(av[5], &data->meals_num) || data->meals_num == 0)
			return (ft_putstr(2, "invalid argument5\n"), 0);
	return (1);
}

void	unlock_all(t_data *data, int nbr)
{
	int	i;

	usleep(2000);
	printf("%.11lld %d died\n", get_rel_time(), nbr);
	i = 0;
	while (i < data->philo_num)
	{
		while (pthread_mutex_destroy(data->mutex + i) != 0)
			;
		i++;
	}
	while (pthread_mutex_destroy(&data->print_mutex) != 0)
		;
}

void	*master(void *ptr)
{
	t_data	*data;
	t_philo	*philos;
	int		i;
	int		stop;

	data = ptr;
	philos = run(data);
	while (usleep(500) == 0 || 1)
	{
		i = 0;
		stop = 1;
		while (i < data->philo_num)
		{
			stop = (stop && data->meals_num <= philos[i].meals_eaten);
			if (get_rel_time() - philos[i].last_meal > data->time_to_die)
			{
				data->out = 1;
				unlock_all(data, i);
				return (philos);
			}
			i++;
		}
		if (stop && data->meals_num != -1)
			return (philos);
	}
}

int	main(int ac, char *av[])
{
	pthread_t	thread;
	t_data		data;
	void		*philos;

	if (ac < 5 || ac > 6)
	{
		if (ac > 0)
			ft_putstr(2, av[0]);
		else
			ft_putstr(2,
				"philo philo_nbr time2die time2eat time2sleep [meals]");
		ft_putstr(2, ": Not enough arguments\n");
		return (1);
	}
	if (data_setup(&data, ac, av) == 0
		|| pthread_create(&thread, NULL, &master, &data) != 0)
		return (1);
	pthread_join(thread, &philos);
	return (free(data.mutex), free(philos), 0);
}
