/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/04 03:19:45 by iait-bel          #+#    #+#             */
/*   Updated: 2022/06/04 03:23:19 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/time.h>
#include <unistd.h>

long	get_time(struct timeval tp)
{
	long	milliseconds;

	milliseconds = tp.tv_sec * 1000;
	milliseconds += tp.tv_usec / 1000;
	return (milliseconds);
}

long	time_diff(struct timeval t1, struct timeval t2)
{
	return (get_time(t2) - get_time(t1));
}

void	ft_usleep(long long n)
{
	struct timeval	start;
	struct timeval	now;
	long			d;

	gettimeofday(&start, NULL);
	usleep(n * .9);
	while (1)
	{
		gettimeofday(&now, NULL);
		d = time_diff(start, now) * 1000;
		if (d >= n)
			break ;
		usleep(50);
	}
}

long long	get_rel_time(void)
{
	static struct timeval	start;
	struct timeval			now;

	if (start.tv_sec == 0)
		gettimeofday(&start, NULL);
	gettimeofday(&now, NULL);
	return (time_diff(start, now));
}
