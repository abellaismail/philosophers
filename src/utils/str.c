/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: iait-bel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/04 03:18:19 by iait-bel          #+#    #+#             */
/*   Updated: 2022/06/05 01:46:19 by iait-bel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include "utils.h"
#include "philo.h"

int	ft_strlen(char *s)
{
	int	i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

void	ft_putstr(int fd, char *s)
{
	write(fd, s, ft_strlen(s));
}

int	append_int(int *res, char c, int sign)
{
	int	_num;
	int	min;

	if (c < '0' || c > '9')
		return (0);
	_num = *res * 10 - (c - '0');
	min = 1 << 31;
	if (_num > *res || (_num == min && sign == 1))
		return (0);
	*res = _num;
	return (1);
}

int	ft_atoi(char *str, int *res)
{
	*res = 0;
	while (*str && append_int(res, *str, 1))
		str++;
	*res *= -1;
	if (*str != 0)
		return (0);
	return (1);
}

void	logging(char *format, t_philo *philo, int is_eat)
{
	if (philo->data->out)
		return ;
	if (is_eat)
		philo->last_meal = get_rel_time();
	pthread_mutex_lock(&philo->data->print_mutex);
	printf(format, get_rel_time(), philo->nbr);
	pthread_mutex_unlock(&philo->data->print_mutex);
}
