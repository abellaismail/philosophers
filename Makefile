CC = cc
CFLAGS = -Wall -Wextra -Werror -g
LDFLAGS = -pthread
INCLUDE = -I./inc
HEADERS = inc/philo.h inc/utils.h

FILES = main gate utils/str utils/time
FILES_BONUS = philo_bonus/main_bonus philo_bonus/simulation_bonus utils/str utils/time philo_bonus/utils

B_DIR = build
OBJ = $(addprefix $(B_DIR)/, $(FILES:=.o))
OBJ_BONUS = $(addprefix $(B_DIR)/, $(FILES_BONUS:=.o))

NAME = philo
NAME_BONUS = philo_bonus

all: $(NAME)
bonus: $(NAME_BONUS)

$(NAME_BONUS): $(OBJ_BONUS)
	$(CC) $(LDFLAGS) $^ -o $@

$(NAME): $(OBJ)
	$(CC) $(LDFLAGS) $^ -o $@

$(B_DIR)/philo_bonus/%.o : philo_bonus/%.c $(HEADERS)
	mkdir -p $(@D)
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@

$(B_DIR)/%.o : src/%.c $(HEADERS)
	mkdir -p $(@D)
	$(CC) $(CFLAGS) $(INCLUDE) -c $< -o $@

clean:
	rm -rf $(B_DIR)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re bonus
